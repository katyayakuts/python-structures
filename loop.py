"""
Calculate n!. n! = 1 * 2 * 3 * … * (n-1) * n,  0! = 1. n >= 0.
"""


def main():
    """Factorial calculation."""
    n = int(input("Please, enter the number \n"))

    f2 = 1
    while n > 0:
        f2 *= n
        n -= 1

    print(f2)


if __name__ == "__main__":
    main()

"""
 Drop empty items from a dictionary.
"""
import json


def main():
    """Drop empty items from a dictionary."""
    d = json.loads(input())

    d = {key: value for (key, value) in d.items() if value is not None}
    print(d)


if __name__ == "__main__":
    main()

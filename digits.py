"""
Find sum of n-integer digits. n >= 0.
"""


def main():
    """Sum of number digits."""
    n = input()

    i = len(n) - 1

    sum = 0
    while i >= 0:
        sum += int(n[i])
        i -= 1

    print(sum)


if __name__ == "__main__":
    main()

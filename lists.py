"""
Consider a list (list = []). You can perform the following commands:
insert i e: Insert integer e at position i.
print: Print the list.
remove e: Delete the first occurrence of integer e.
append e: Insert integer e at the end of the list.
sort: Sort the list.
pop: Pop the last element from the list.
reverse: Reverse the list.

Initialize your list and read in the value of followed by lines of commands
where each command will be of the  types listed above. Iterate through each command
in order and perform the corresponding operation on your list.
The first line contains an integer, denoting the number of commands.
Each line  of the  subsequent lines contains one of the commands described above.
"""


def main():
    """Perform list commands."""
    n = int(input())

    lst = []
    lst_res = []
    pos = []

    for i in range(0, n):
        ele = input()

        lst.append(ele)

    for i in range(0, n):
        command = str(lst[i])
        if command.find('insert') != -1:
            pos = command.split()
            lst_res.insert(int(pos[1]), int(pos[2]))
        elif command.find('remove') != -1:
            pos = command.split()
            lst_res.remove(int(pos[1]))
        elif command.find('append') != -1:
            pos = command.split()
            lst_res.append(int(pos[1]))
        elif command.find('print') != -1:
            print(lst_res)
        elif command.find('sort') != -1:
            lst_res.sort()
        elif command.find('pop') != -1:
            lst_res.pop()
        elif command.find('reverse') != -1:
            lst_res.reverse()


if __name__ == "__main__":
    main()

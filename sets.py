"""
Find common items in 2 lists without duplicates. Sort the result list before output.
"""


def main():
    """Find common numbers."""
    li1 = list(map(int, input().split()))
    li2 = list(map(int, input().split()))

    li1.sort()
    li2.sort()

    fl = 0
    i = 0

    if len(li1) <= 1 or len(li2) == 0:
        fl = 1
    else:
        while fl == 0:
            if li1[i] == li1[i + 1]:
                del li1[i]
            i += 1
            if i >= len(li1) - 1:
                fl = 1

    result = []
    for sym in li1:
        if sym in li2:
            result.append(sym)

    print(result)
    # res2 = []
    # res2 = [eval(i) for i in result]
    # print(res2)


if __name__ == "__main__":
    main()

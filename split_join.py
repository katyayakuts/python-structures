"""
Given a string and create a reverse
"""


def main():
    """Check palindrome."""
    s = input()
    s_new = ''

    s_new = s.split()

    answer = ''
    for sym in s_new:
        if len(answer) == 0:
            answer = answer + str(sym[::-1])
        else:
            answer = answer + ' ' + str(sym[::-1])

    print(answer)


if __name__ == "__main__":
    main()
